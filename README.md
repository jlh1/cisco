# Mkdocs Local y en GitLab para documentar

## Creamos el entorno local:

```bash
python3 -m venv mkdocs_env
```

Activamos el entorno:

```bash
source mkdocs_env/bin/activate
```

Instalamos el generador de páginas estáticas MkDocs:
```bash
pip install mkdocs
```
Instalamos también una plantilla para nuestro sitio:
```bash
pip install mkdocs-material
```

creamos carpeta para el sitio del proyecto
```bash
mkdocs new my_docu

cd my_docu
```

## Crear y sincronizar con repo de GitLab

```bash
Push an existing folder
cd existing_folder
git init
git remote add origin git@gitlab.com:labredes/cisco.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

## Crear CI/CD

[Crear una Pagina web en GitLab usando CI/CD template](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_ci_cd_template.html)

[Ver Resultado de Documentación desde GitLab](https://unrc.gitlab.io/laboratorio_redes/tutoriales/)

## Referencias

- [mkdocs](https://www.mkdocs.org/)

- [mkdocs-material](https://squidfunk.github.io/mkdocs-material/)

- [GitLabPages](https://docs.gitlab.com/ee/user/project/pages/)
