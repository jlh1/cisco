# Cuardar Configuración de equipo Cisco

Por defecto, al momento de apagar un equipos Cisco, la configuración que se ha implementado durante la seción de trabajo se borra. 
Para guardar la configuración, se pueden implementar las siguientes opciones:

```bash
equipo# copy running-config startup-config
```

Este es el comando que guarda los cambios realizados en la configuración activa (RAM) a un archivo de configuración de respaldo (NVRAM ) que es el que será utilizado en caso de que por cualquier motivo el dispositivo sea reiniciado.

Modo antiguo:

```bash
equipo# write memory
```
