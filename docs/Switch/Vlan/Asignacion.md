#Asignación de puertos

## Puerto en modo acceso

```
Switch(config)# interface "interfaz" "#i"
Switch(config-if)# switchport mode access
Switch(config-if)# switchport access vlan "#v"
```

Donde: | |
-- | --
**interfaz** = nombre lógico de la interfaz a configurar | **#i** = número de interfaz
**#v** = número de vlan a la que se accede por la interfaz |

## Puerto en modo troncal

Cabecera A | Cabecera B
-- | --
 Permite el paso de todas las vlan: | Switch(config)# interface "interfaz" "#i" 
 . | Switch(config-if)# switchport mode trunk
 . | Switch(config-if)# switchport trunk allowed vlan all
Suma una vlan a las permitidas: | Switch(config-if)# switchport trunk allowed vlan add "#v"
Quita una vlan de las permitidas: | Switch(config-if)# switchport trunk allowed vlan remove "#v"
Permite todas excepto #vlan:  | Switch(config-if)# switchport trunk allowed vlan exept "#v"
No permite el paso de tramas etiquetadas: | Switch(config-if)# switchport trunk allowed vlan none|


Donde: | |
-- | --
**interfaz** = nombre lógico de la interfaz a configurar | **#v** = número de vlans que se permiten en la interfaz
**#i** = número de interfaz |

### Protocolo de encapsulación en el troncal
Los switch 2950 solo admiten 802.1q, por lo que no se debe configurar el tipo de encapsulación. 
En los switches 3550 se debe elegir entre ISL ó dot1q (802.1Q).

```
Switch-3550(config)# interface interfaz "#i"
Switch-3550(config-if)# switchport trunk encapsulation encapsulación
Switch-3550(config-if)# switchport mode trunk
```

Donde: | |
-- | --
**interfaz** = nombre lógico de la interfaz a configurar | **encapsulación** = modo de encapsulación, para la interacción con los switch 2950 elegir dot1q
**#i** = número de interfaz |
