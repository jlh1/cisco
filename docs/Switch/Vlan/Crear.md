## Crear una vlan

```bash
Switch(config)# vlan "#v"
Switch(config-vlan)# name "nombre"
```

Donde: | |
-- | --
**#v** = número de vlan | **nombre** = nombre de la vlan
