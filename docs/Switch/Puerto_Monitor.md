# Puerto en modo monitor

```bash
Switch(config)# monitor session "x" source interface "y"
Switch(config)# monitor session "x" destination interface "z"
Switch#sh monitor session all
```

Donde: | |
-- | --
**x** = número de sesión | **y** = nombre lógico de la interfaz a capturar
**z** = nombre lógico de la interfaz desde donde se visualiza el tráfico | |

SPAN (Switch Port Analyzer). Dentro de la sesión x se monitorea las interfaces y desde la interfaz z. Un puerto monitor solo reproduce la comunicación de capa 3 y superiores.
