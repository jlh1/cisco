# Cliente DHCP 

Al configurar una interfaz como cliente DHCP, se puede obtener en forma automática los siguientes parámetros:

* Dirección IP y máscara de red.
* Puerta de enlace.
* Servidor de DNS.

## Configurar interfaz como cliente DHCP

```bash
Router(config)#interface "interfaz" "#i"
Router(config-if)# ip address dhcp
```

Donde: | |
-- | --
**interfaz #** = nombre lógico de la interfaz que se quiere configurar por dhcp | **#i** = número de interfaz
**dhcp** = opción que habilita ser cliente dhcp |

## Visualizar ip asignada

```bash
Router# show ip interface brief
Interface     IP-Address     OK?     Method     Status     Protocol
"interfaz"    "x.x.x.x"      YES     "DHCP"     up         up
```

Donde: | |
-- | --
**interfaz** = nombre lógico de la interfaz | **x.x.x.x** = dirección IP
**DHCP** = indica que se obtuvo por dhcp | 
