# VLAN - Ruteo de VLANs

## Ruteo entre vlans
La asignación de dirección IP se configura dentro de una subinterfaz dependiente del identificador de la vlan en cuestión:

```bash
Router(config)# interface "interfaz" "#i"."#v"
Router(config-subif)# encapsulation proto "#v"
Router(config-subif)# ip address "x.x.x.x" "y.y.y.y"
```

Donde: | |
-- | --
**interfaz** = nombre lógico de la interfaz | **#i** = número de interfaz
**#v** = número de vlan | **proto** = protocolo para encapsulación (dot1Q)
**x.x.x.x** = dirección de red a alcanzar | **y.y.y.y** = máscara de red
