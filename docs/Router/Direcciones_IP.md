# Direcciones IP

## Visualización de direcciones IP configuradas 

Con el siguiente comando se pueden visualizar las direcciones IP asignadas a cada interfaz:

```bash
Router# show ip interface brief
```

## Asignación de dirección IP a una interfaz 

```bash
Router(config)# interface "interfaz" "#i"
Router(config-if)# ip address "x.x.x.x" "y.y.y.y"
Router(config-if)# no shutdown
```

Donde: | | |
-- | -- | --
**interfaz** = nombre lógico de la interfaz | **#i** = número de interfaz | **x.x.x.x** = dirección IP 
**y.y.y.y** = máscara de red  | **no shutdown** = habilita la interfaz | 


En caso que se quiera adicionar más de una misma dirección IP a una interfaz:

```bash
Router(config-if)# ip address "x.x.x.x" "y.y.y.y" secondary
```

Donde: | |
-- | --
**x.x.x.x** = dirección IP | **secondary** = agrega dirección IP por sobre las que se encuentran configuradas 
**y.y.y.y** = máscara de red | 

## Eliminar direcciones IP asignadas a una interfaz

```bash
Router(config)# interface "interfaz #"
Router(config-if)# no ip address "x.x.x.x" "y.y.y.y"
```

Donde: | | |
-- | -- | --
**interfaz #** = nombre lógico de la interfaz | **x.x.x.x** = dirección IP | **y.y.y.y** = máscara de red

## Configuración de Interfaces SERIALES

```bash
Router(config)# interface serial "#i"
Router(config-if)# ip address "x.x.x.x" "y.y.y.y"
Router(config-if)# clock rate "u"
Router(config-if)# "no shutdown"
```

Donde: | |
-- | -- 
**#i** = ubicación de la interfaz serial en el router | 
**x.x.x.x** = dirección IP | **y.y.y.y** = máscara de red 
**u** = velocidad a setear en la conexión serial | Utilizar **?** para ver las opciones de velocidad
