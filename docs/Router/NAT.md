# NAT

Al configurar NAT, se puede realizar un mapeo de las direcciones IP y puertos de la interfaz de entrada y salida.

A continuación se muestra un modo de configuración de NAT para enmascarar la dirección IP de origen de paquetes en función a la IP de una interfaz de salida. Los pasos son los siguientes:

1- Se debe generar una lista de acceso, para indicar las direcciones IP que se quieren enmascarar. Para ello, se indica la dirección IP de red y la máscara de red invertida.

2- Interfaz cuya IP se utilizará para enmascarar.

3- Interfaz por donde ingresan los paquetes.

4- Interfaz por donde salen los paquetes.

## Configuración de NAT

```bash
Router(config)# access-list 101 permit ip "x.x.x.x" "y.y.y.y" any
Router(config)# ip nat inside source list 101 interface "interfaz_y" overload
Router(config)# interface "interfaz_x"
Router(config-if)# ip nat inside
Router(config)# interface "interfaz_y"
Router(config-if)# ip nat outside
```

Donde: | |
-- | --
**x.x.x.x** = dirección de RED a ocultar | **y.y.y.y** = wildmask o máscara invertida (que tiene 0 en bits de RED y 1 en los bits HOST )
**interfaz_x** = interfaz correspondiente a la red a ocultar | **interfaz_y** = interfaz cuya IP se utilizará para la traslación

Ejemplo: trasladar las direcciones IP de origen de los paquetes de la red 192.168.1.0/24 a la dirección IP 192.168.2.1:
```bash
Router(config)# access-list 101 permit ip 192.168.1.0 0.0.0.255 any
Router(config)# ip nat inside source list 101 interface fastEthernet 0/1 overload
Router(config)# interface fastEthernet 0/0
Router(config-if)# ip nat inside
Router(config)# interface fastEthernet 0/1
Router(config-if)# ip nat outside
```

## Visualización de tabla NAT
```bash
Router# show ip nat translations

"Pro"        "Inside global"     "Inside local"     "Outside local"     "Outside global"
```

Donde: | |
-- | --
**Pro** = protocolo de capa superior | **Inside global** = Dirección IP origen y puerto luego del nat
**Inside local** = Dirección IP origen y puerto antes del nat | **Outside local** = Dirección IP destino y puerto antes del nat
**Outside global** = Dirección IP destino y puerto luego del nat |
