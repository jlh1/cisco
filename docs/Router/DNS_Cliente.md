#Cliente DNS 

```bash
Router(config)# ip name-server "d.d.d.d"
```

Donde: | |
-- | --
**d.d.d.d** = dirección IP del servidor DNS | |
