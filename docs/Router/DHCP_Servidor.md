# Servidor DHCP 

## Reserva de un pool de direcciones

En esta sección se indican los parámetros de red que se ha de configurar a los clientes DHCP:

* Dirección IP

* Puerta de enlace

* Servidor de DNS

```
Router(config)# ip dhcp pool "nombre"
Router(dhcp-config)# network "x.x.x.x" "y.y.y.y"
Router(dhcp-config)# default-router "z.z.z.z"
Router(dhcp-config)# dns-server "d.d.d.d"
```

| Donde: | |
| -- | -- |
| **nombre** = nombre del pool | |
| **x.x.x.x** = dirección IP de la red | **y.y.y.y** = máscara de red |
| **d.d.d.d** = dirección IP del servidor DNS | **z.z.z.z** = dirección IP de la puerta de enlace |

## Exclusión de direcciones IP

Es posible reservar direcciones IP dentro de la red que se ha de configurar en el servidor DHCP para usos particulares o para asignar de forma manual. Para ello se puede ejecutar:

```
Router(config)# ip dhcp excluded-address "e.e.e.e" "f.f.f.f"
```

| Donde: | |
| -- | -- |
| **e.e.e.e** = Nº IP inicial para excluir | **f.f.f.f** = Nº IP final para excluir |

## Visualización de configuración del servidor DHCP
```
Router# show ip dhcp pool
```

## Visualización de configuración asignada a clientes

```
Router# show ip dhcp binding
```
