# Modos de Acceso

La configuración del equipo se puede hacer mediante conexión:

* directa a la consola a través del puerto serie del Cisco y la computadora
* remota a la consola por telnet a una dirección IP previamente configurada, conocida y accesible.
* remota segura a la consola por SSH a una dirección IP previamente configurada, conocida y accesible.
* remota Web. En un entorno Java pesado que no contiene todas las opciones posibles de configuración.

## Asignación de un nombre un equipo
```bash
equipo#configure terminal
equipo(config)#hostname nombre
nombre(config)#
```

Donde: | |
-- | --
**nombre** = nombre a configurar en el equipo | |

## Gestión de contraseñas

### Consola - Contraseña compartida
```bash
equipo(config)#line console 0
equipo(config-line)#password "contraseña"
equipo(config-line)#login 
```

Donde: | |
-- | --
**contraseña** = contraseña asignada a la consola | |

### Terminal de configuración
```bash
equipo(config)# enable secret "contraseña"
```

Donde: | |
-- | --
**contraseña** = contraseña asignada al terminal | |

### Telnet
Importante: para implementar telnet es necesario tener configurada una contraseña en el terminal de configuración.

#### Contraseña compartida
Acceso a configuración telnet:
Carga de contraseña compartida:
Habilitar la autenticación:
```bash
equipo(config)#line vty 0 15
equipo(config-line)#password contraseña
equipo(config-line)#login 
```

Donde: | |
-- | --
**contraseña** = contraseña asignada a la conexión telnet | |

#### Múltiples usuarios con sus respectivas contraseñas
Carga de la asociación usuario y contraseña:
Acceso a configuración telnet:
No se utiliza contraseña compartida:
Autenticación con usuario/contraseña:
```bash
equipo(config)#username "nombrei" secret "contraseñai"
equipo(config)#line vty 0 15
equipo(config-line)#no password
equipo(config-line)#login local
```

Donde: | |
-- | --
**nombrei**  = nombre asignada al usuario i | **contraseñai** = contraseña asignada al usuario i

### Visualizar usuarios conectados
```bash
equipo#show users
```

### Ver el estado de la configuración en el equipo
```bash
equipo#show running-config

equipo#show ip interfaces brief
```
